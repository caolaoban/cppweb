#include <webx/menu.h>

class GetUserIcon : public webx::ProcessBase
{
protected:
	int process();
};

HTTP_WEBAPP(GetUserIcon)

int GetUserIcon::process()
{
	param_string(user);

	SmartBuffer content;

	if (webx::IsFileName(user))
	{
		static set<string> extset = {"png", "jpg", "ico", "bmp", "gif", "jpeg"};
		string path = stdx::format("pub/usr/pic/%03d/", MD5GetUINT32(user.c_str(), user.length()) % 1000);

		path = app->getPath() + path + user;

		for (const string& extname : extset)
		{
			if (app->getFileContent(path + "." + extname, content) > 0)
			{
				response->setContentType(app->getMimeType(extname));

				break;
			}
		}
	}

	if (content.isNull())
	{
		if (app->getFileContent(app->getPath() + "res/img/user/user.png", content) <= 0xFF) return XG_NOTFOUND;

		response->setContentType(app->getMimeType("png"));
	}

	return createFile(content.size()) ? file->write(content.str(), content.size()) : XG_NOTFOUND;
}