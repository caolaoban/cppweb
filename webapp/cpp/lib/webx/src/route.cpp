#ifndef XG_WEBX_ROUTE_CPP
#define XG_WEBX_ROUTE_CPP
///////////////////////////////////////////////////////////
#include "../menu.h"
#include "../route.h"

int webx::GetLastRemoteStatus()
{
	typedef int (*GetRemoteStatusFunc)();
	static GetRemoteStatusFunc func = (GetRemoteStatusFunc)Process::GetObject("HTTP_GET_LAST_REMOTE_STATUS_FUNC");

	return func ? func() : XG_FAIL;
}
HostItem webx::GetRegCenterHost()
{
	int port;
	char host[64];
	HostItem item;
	typedef int (*GetRegCenterHostFunc)(char*, int*);
	static GetRegCenterHostFunc func = (GetRegCenterHostFunc)Process::GetObject("HTTP_GET_REG_CENTER_HOST_FUNC");

	if (func == NULL) return item;

	if (func(host, &port) > 0)
	{
		item.host = host;
		item.port = port;
	}

	return item;
}

void webx::CheckSystemRight(ProcessBase* proc)
{
	try
	{
		proc->checkLogin();
		proc->checkSystemRight();
	}
	catch(Exception e)
	{
		HostItem route = GetRegCenterHost();
		const string& path = proc->getRequest()->getPath();
		const string& host = proc->getResponse()->getClientHost();

		if (host.empty()) return stdx::Throw(XG_AUTHFAIL, "permission denied");

		if (IsLocalHost(host.c_str()))
		{
			LogTrace(eIMP, "request[%s] from local host", path.c_str());
		}
		else if (route.equals(host))
		{
			LogTrace(eIMP, "request[%s] from route center", path.c_str());
		}
		else
		{
			sp<DBConnect> dbconn = webx::GetDBConnect();
			sp<QueryResult> rs = dbconn->query("SELECT ID FROM T_XG_ROUTE WHERE HOST=? AND ENABLED>0", host);

			if (rs && rs->next())
			{
				LogTrace(eIMP, "request[%s] from trust host[%s]", path.c_str(), host.c_str());
			}
			else
			{
				throw e;
			}
		}
	}
}

HostItem webx::GetRouteHost(const string& path)
{
	int port;
	char host[64];
	HostItem item;
	typedef int (*GetRouteHostFunc)(const char*, char*, int*);
	static GetRouteHostFunc func = (GetRouteHostFunc)Process::GetObject("HTTP_GET_ROUTE_HOST_FUNC");

	if (func == NULL) return item;

	if (func(path.c_str(), host, &port) > 0)
	{
		item.host = host;
		item.port = port;
	}

	return item;
}

int webx::UpdateRouteList(const string& host, int port)
{
	typedef int (*UpdateRouteListFunc)(const char*, int);
	static UpdateRouteListFunc func = (UpdateRouteListFunc)Process::GetObject("HTTP_UPDATE_ROUTE_LIST_FUNC");

	if (func == NULL) return XG_SYSERR;

	return func(host.c_str(), port);
}

SmartBuffer webx::GetRemoteResult(const string& path, const string& param, const string& contype, const string& cookie)
{
	typedef SmartBuffer (*GetRemoteResultFunc)(const char*, const char*, const char*, const char*);
	static GetRemoteResultFunc func = (GetRemoteResultFunc)Process::GetObject("HTTP_GET_REMOTE_RESULT_FUNC");

	if (func == NULL) return SmartBuffer();

	return func(path.c_str(), param.c_str(), contype.c_str(), cookie.c_str());
}

int webx::Broadcast(const string& path, const string& param, const string& contype, const string& cookie)
{
	typedef int (*BroadcastFunc)(const char*, const char*, const char*, const char*);
	static BroadcastFunc func = (BroadcastFunc)Process::GetObject("HTTP_BROADCAST_HOST_FUNC");

	if (func == NULL) return XG_SYSERR;

	return func(path.c_str(), param.c_str(), contype.c_str(), cookie.c_str());
}

int webx::NotifyHost(const string& host, int port, const string& path, const string& param, const string& contype, const string& cookie)
{
	typedef int (*NotifyHostFunc)(const char*, int, const char*, const char*, const char*, const char*);
	static NotifyHostFunc func = (NotifyHostFunc)Process::GetObject("HTTP_NOTIFY_HOST_FUNC");

	if (func == NULL) return XG_SYSERR;

	return func(host.c_str(), port, path.c_str(), param.c_str(), contype.c_str(), cookie.c_str());
}
///////////////////////////////////////////////////////////
#endif
