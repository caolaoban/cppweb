-- compiler
------------------------------------------------------------------
CC = "g++ -std=c++11"

-- compile setting
------------------------------------------------------------------
FLAG = "-DXG_WEBX_CPPSHELL -I. -I./inc -I$SOURCE_HOME/library -I$PRODUCT_HOME/lib/openssl/inc -I$SOURCE_HOME/webapp/cpp/lib"

-- library link setting
------------------------------------------------------------------
LIB_LINK = "-L$PRODUCT_HOME/lib -lwebx -lhttp -lweb.dbentity -ldbx.base -lopenssl -lzlib -ljson -lstdx -lclib -L$PRODUCT_HOME/lib/openssl/lib -lssl -lcrypto"