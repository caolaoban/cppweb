#ifndef XG_HEADER_NETX
#define XG_HEADER_NETX
//////////////////////////////////////////////////////////
#include "system.h"

#ifndef HOST_IP
#define HOST_IP						"0.0.0.0"
#endif

#ifndef LOCAL_IP
#define LOCAL_IP					"127.0.0.1"
#endif

#ifndef SOCKECT_FRAMESIZE
#define SOCKECT_FRAMESIZE			8 * 1024
#endif

#ifndef SOCKECT_RECVTIMEOUT
#define SOCKECT_RECVTIMEOUT			10
#endif

#ifndef SOCKECT_SENDTIMEOUT
#define SOCKECT_SENDTIMEOUT			20
#endif

#ifndef SOCKET_CONNECT_TIMEOUT
#define SOCKET_CONNECT_TIMEOUT		3000
#endif

#ifndef SOCKET_TIMEOUT_LIMITSIZE
#define SOCKET_TIMEOUT_LIMITSIZE	10
#endif

#ifndef SOCKET_TIMEOUT_REDOTIMES
#define SOCKET_TIMEOUT_REDOTIMES	100
#endif

#ifdef __cplusplus
extern "C" {
#endif
	inline static void SocketSetup()
	{
#ifdef XG_LINUX
		signal(SIGPIPE, SIG_IGN);
#else
		WSADATA data; WSAStartup(MAKEWORD(2, 2), &data);
#endif
	}

	BOOL IsSocketTimeout();

	void SocketClose(SOCKET sock);

	BOOL IsSocketClosed(SOCKET sock);

	int GetLocalAddress(char* host[]);
	
	BOOL IsLocalHost(const char* host);

	int SocketWriteEmptyLine(SOCKET sock);

	BOOL SocketSetSendTimeout(SOCKET sock, int ms);

	BOOL SocketSetRecvTimeout(SOCKET sock, int ms);

	SOCKET SocketConnect(const char* ip, int port);

	BOOL GetSocketAddress(SOCKET sock, char* address);

	int SocketReadLine(SOCKET sock, char* msg, int len);

	SOCKET ServerSocketAccept(SOCKET svr, char* address);

	const char* GetHostAddress(const char* host, char* ip);

	int SocketWriteLine(SOCKET sock, const char* msg, int len);

	SOCKET CreateServerSocket(const char* ip, int port, int backlog);

	SOCKET SocketConnectTimeout(const char* ip, int port, int timeout);

	int SocketPeek(SOCKET sock, void* data, int size);

	int SocketRead(SOCKET sock, void* data, int size);

	int SocketWrite(SOCKET sock, const void* data, int size);

	int SocketReadEx(SOCKET sock, void* data, int size, BOOL completed);

	int SocketWriteEx(SOCKET sock, const void* data, int size, BOOL completed);

	typedef struct
	{
		char flag[8];
		char addr[32];
	} stConnectData;

	BOOL ServerSocketAttach(SOCKET sock, const char* address);

	void ServerSocketSetLockFunction(void(*lock)(), void(*unlock)());

	void ServerSocketSetProcessFunction(int(*func)(SOCKET, stConnectData*));

	void ServerSocketLoop(const char* host, int port, int backlog, int timeout);

	void ServerSocketSetConnectClosedFunction(int(*func)(SOCKET, stConnectData*));
	
	void ServerSocketSetConnectFunction(int(*func)(SOCKET, stConnectData*, const char*, int));
#ifdef __cplusplus
}
#endif

//////////////////////////////////////////////////////////
#endif