#include <http/HttpHelper.h>

int main(int argc, char** argv)
{
	Process::Instance(argc, argv);

	if (Process::GetCmdParamCount() < 2)
	{
		ColorPrint(eRED, "please input address\n");
 
		return XG_PARAMERR;
	}

	SmartBuffer data = HttpHelper::GetResult(argv[1]);

	if (data.isNull())
	{
		ColorPrint(eRED, "grasp content failed\n");
 
		return XG_PARAMERR;
	}

	fwrite(data.str(), sizeof(char), data.size(), stdout);
	fflush(stdout);

	return 0;
}